const header__burger = document.querySelector('.header-burger__burger');
const header__menu = document.querySelector('.header-burger__menu')
const header__list = document.querySelector('.header-burger__list')
const header__btn = document.querySelector('#header-burger__btn')
const header__logo = document.querySelector('.header-burger__logo')
const squadArrow = document.querySelector('.squad-arrow')
const squadList = document.querySelectorAll('.squad-col')
const form = document.querySelector('.form')
const select = document.querySelector('._sel')
const options = document.querySelectorAll('.form-select__option')
const btnDown = document.querySelector('.intro-down')
const header = document.querySelector('.header')

const models3d = document.querySelectorAll('.intro-model3d')
const spinners = document.querySelectorAll('.intro-preloader')
const spinner = (document.documentElement.clientWidth < 575) ? spinners[1] : spinners[0]
const model3D = (document.documentElement.clientWidth < 575) ? models3d[1] : models3d[0]
const displayValue = {
    none: 'none',
    flex: 'flex'
}
model3D.addEventListener('load', () => {
    spinner.style.display = displayValue.none
    model3D.style.display = displayValue.flex
})


const telegramInput = document.querySelector('#telegram')

telegramInput.addEventListener('input',(event)=>{
    let value = event.target.value
    if (value[0] !== '@') {
        telegramInput.value = `${"@"}${value}`
    }
})

telegramInput.addEventListener('keyup',(event)=>{
    let value = event.target.value
    if (event.keyCode === 8 && value[0] === '@' && !value[1]) {
        telegramInput.value = ''
    }
})

var Visible = function (target) {

    var targetPosition = {
            top: window.pageYOffset + target.getBoundingClientRect().top,
            left: window.pageXOffset + target.getBoundingClientRect().left,
            right: window.pageXOffset + target.getBoundingClientRect().right,
            bottom: window.pageYOffset + target.getBoundingClientRect().bottom
        },

        windowPosition = {
            top: window.pageYOffset,
            left: window.pageXOffset,
            right: window.pageXOffset + document.documentElement.clientWidth,
            bottom: window.pageYOffset + document.documentElement.clientHeight
        };

    if (targetPosition.bottom > windowPosition.top &&
        targetPosition.top < windowPosition.bottom) {

        header.style.position = 'absolute'
    } else {
        header.style.position = 'fixed'
    }
}

window.addEventListener('scroll', function () {
    Visible(btnDown);
});

Visible(btnDown);

function toggleActive(elem) {
    elem.classList.toggle('active')
}

header__burger.addEventListener('click', () => {
    toggleActive(header__burger)
    toggleActive(header__menu)
    toggleActive(header__btn)
    toggleActive(header__logo)


    document.body.classList.toggle('lock')
})

header__list.addEventListener('click', () => {
    header__burger.classList.remove('active')
    header__menu.classList.remove('active')
    document.body.classList.toggle('lock')
})

squadArrow.addEventListener('click', () => {
    squadList.forEach((item, index) => {
        if (index >= 4) {
            item.classList.toggle('squad-col-show')
        }
    })
    squadArrow.classList.toggle('squad-arrow-open')
})

form.addEventListener("submit", formSend)

async function formSend(e) {
    e.preventDefault()

    let error = formValidate()
    const submitForm = document.querySelector('.form-submit')
    let formData = new FormData(form)

    if (error === 0) {
        submitForm.disabled = true
        try {
            let response = await fetch("https://karazina-environment.herokuapp.com/email", {
                method: 'POST',
                body: formData
            })
            if (response.ok) {
                alert('Ваш запрос отправлен')
                form.reset()
            }
        } catch (e) {
            alert("Ошибка Запроса")
        } finally {
            submitForm.disabled = false
        }
    }
}

function formValidate() {
    let error = 0
    let formReq = document.querySelectorAll('._req')
    formReq.forEach(item => {
        formRemoveError(item)
        if (item.classList.contains('_email')) {
            if (emailTest(item)) {
                formAddError(item)
                error++
            }
        } else if ((item.classList.contains('_sel')) && (item.value === options[0].value)) {
            formAddError(item)
            error++
        } else if (item.classList.contains('_tel')) {
            if (telegramTest(item)) {
                formAddError(item)
                error++
            }
        } else {
            if (item.value === "") {
                formAddError(item)
                error++
            }
        }

    })
    return error
}

function formAddError(item) {
    item.classList.add('_error')
}

function formRemoveError(item) {
    item.classList.remove('_error')
}

function emailTest(item) {
    return !/^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/.test(item.value)
}

function telegramTest(item) {
    return ! /^@?[A-Za-z0-9_.]+.{4,}/.test(item.value)
}
function scrollToBlock(divName, offset = 75) {
    var elementPosition = document.querySelector(divName).offsetTop;
    window.scrollTo({
            top: elementPosition - offset,
            behavior: "smooth"
        }
    );
}

$('.owl-carousel').owlCarousel({
    center: true,
    items: 2,
    loop: true,
    responsive: {
        0: {
            stagePadding: 20,
            items: 1
        },
        769: {
            nav: true,
            items: 1.5
        }
    },

    lazyLoad: true,
    responseiveRefreshRate: 50,
    autoplay: true,
    autoplayTimeout: 5000,
    autoplayHoverPause: true
});
